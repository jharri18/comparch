#include <stdio.h>
#include <math.h>

#define XSIZE 100
#define YSIZE 100
int hits, misses;

void calcVal(int b){
	unsigned maskOne = 0xF00, maskTwo = 0x0F0, maskThree=0x00F;
	int one = maskOne & b;
	int two = maskTwo & b;
	int three = maskThree & b;
	printf("Item Recieved: %x\n", b);	

	printf("Components %d,%d,%d\n",one,two,three);

	int sum = one+two+three;
	printf("Sum: %d\n",sum);

	
	

}
	

void checkHits(int a[], int b){
	unsigned int mask = 0x00000000FFF0;
	printf("Mask: %p\n", mask);
	printf("%p\n",b);

	int extract = b & mask;
	printf("%p\n", extract);

	calcVal(extract >> 4);
}

int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];
  int cache[1024];

  int i, j, k;
  
  int r;
//printf("\n\nMatrix X:\n");
  /* Initialize x matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      x[i][j] = 0;
  //    printf("Row %d, Col %d: %p\n",i,j, &x[i][j]);
    }
  }
//printf("\n\nMatrix Y:\n");
  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j;
  //    printf("Row %d, Col %d: %p\n",i,j, &y[i][j]);
    }
  }
//printf("\n\nMatrix Z\n");
  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
	z[i][j] = i + j;
//	printf("Row %d, Col %d: %p\n",i,j, &z[i][j]);
    }
  }

  int one_time = 0;
  /* Do matrix multiply */
  for(i=0; i<XSIZE; i=i+1) {
    for(j=0; j<YSIZE; j=j+1) {
      r = 0;
      for(k=0; k<XSIZE; k=k+1) {
	if(one_time == 0){
		one_time = 1;
		checkHits(cache[1024], &y[i][k]);
	}

        r = r + y[i][k] * z[k][j];
      }
      x[i][j] = r;
    }
  }
}
