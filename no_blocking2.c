#include <stdio.h>
#include <math.h>

#define XSIZE 100
#define YSIZE 100
int hits = 0, misses = 0;
int cacheTag[2048] = {0}, cacheValid[2048] = {0};

int calcVal(int b){

	if(hits+misses<1){
		printf("Trial: %d\n", b/2048);
		printf("Item Recieved: %x\n", c);	
	}
	//printf("Sum: %d\n",sum);
	
	return b%2048;
}
	

void checkHits1(int b){
	unsigned int indexMask = 0x1FFC0;
	unsigned int tagMask = 0xFFFE00000;
//	printf("%p\n",b);

	int extract = b & indexMask;
//	printf("Index:  %p\n", extract);

	int index = calcVal(extract >> 4);
	unsigned int extractTag = ((b & 0xFFFF0000)>>16) & 0xFFFE;
	unsigned int tagMask2 = extractTag;
	if(hits+misses <1){
		printf("Full addr %p\n", b);
		printf("Extracted Index: %p\n", extract>>4);
	}
	if(cacheValid[index] == 0){
		cacheValid[index] = 1;
		cacheTag[index] = tagMask2;
		if(misses<20){
//			printf("Misses: %d, Tag %d\n", index, tagMask2); 
		}
		misses += 1;
		return;
	}

	if(cacheTag[index] == tagMask2){
		if(hits < 20){
//			printf("Hit: %d, Tag: %d\n", index, tagMask2);
		}
		hits +=1;
		return;
	}

        if(misses<20){
//	        printf("Misses: %d, Tag %d\n", index, tagMask2);
        }

	cacheTag[index] = tagMask2;
	misses+=1;


}


void checkHits(int b){
	unsigned int mask = 0x00000000FFF0;
	unsigned int tagMask = 0xFFFF80000;
	//printf("Mask: %p\n", mask);
	//printf("%p\n",b);

	int extract = b & mask;
	//printf("%p\n", extract);

	int index = calcVal(extract >> 4);
	int extractTag = b & tagMask;
	unsigned int tagMask2 = (extractTag >> 16) & 0x000FFFFF;
	//printf("%p\n", tagMask2);

	if(cacheValid[index] == 0){
		cacheValid[index] = 1;
		cacheTag[index] = tagMask2;
		if(misses<20){
//			printf("Misses: %d, Tag %d\n", index, tagMask2); 
		}
		misses += 1;
		return;
	}

	if(cacheTag[index] == tagMask2){
		if(hits < 20){
//			printf("Hit: %d, Tag: %d\n", index, tagMask2);
		}
		hits +=1;
		return;
	}

        if(misses<20){
//	        printf("Misses: %d, Tag %d\n", index, tagMask2);
        }

	cacheTag[index] = tagMask2;
	misses+=1;


}

int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];


  int i, j, k;
  
  int r;
  
//printf("\n\nMatrix X:\n");
  /* Initialize x matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      x[i][j] = 0;
	    //printf("Row %d, Col %d: %p\n",i,j, &x[i][j]);
    }
  }
//printf("\n\nMatrix Y:\n");
  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j;
  //    printf("Row %d, Col %d: %p\n",i,j, &y[i][j]);
    }
  }
//printf("\n\nMatrix Z\n");
  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
	z[i][j] = i + j;
//	printf("Row %d, Col %d: %p\n",i,j, &z[i][j]);
    }
  }


  //int first =0;
  /* Do matrix multiply */
  for(i=0; i<XSIZE; i=i+1) {
    for(j=0; j<YSIZE; j=j+1) {
      r = 0;
      for(k=0; k<XSIZE; k=k+1) {
	//printf("%p\n", &y[i][k]);
//	if(first == 0){
//		printf("%x\n", &y[i][k]);
//		first = 1;
	        checkHits1(&y[i][k]);
//	}
	checkHits1(&z[k][j]);
        r = r + y[i][k] * z[k][j];
      }
      
      checkHits1(&x[i][j]);
      x[i][j] = r;
    }
  }

	int total = hits + misses;
	printf("Total checks: %d\n", total);
	printf("Misses: %d \n", hits,misses);
}
