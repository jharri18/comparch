LAB3 Question2

From what I calculated there was going to be 2^10 blocks in the cache.
The right 5 most bits were the offset, next 10 bits were index, and whatever is left was the tag.

Basically what I have so far was an attempt at filtering out the index bits from the address because thats really all we care about here I think.  Within the calcVal function, that sum variable is the digit representation of the 10 bits value that will get the index.  My idea is we would do sum%1024 in order to calculate an index within the cache. Then we would populate the cache array with starting addresses of blocks and check this array each time we attempt to access memory elements.

If this is a dumb approach feel free to make changes/start over on this problem. 
